# Profiling

El profiling, en el contexto del desarrollo de software y la ingeniería de sistemas, es una forma esencial de análisis de rendimiento que se enfoca en la medición de la complejidad computacional, el uso de recursos y el desempeño de un programa. Esta práctica es de suma importancia por varias razones:

- **Optimización de recursos**: Permite a los desarrolladores identificar las secciones de código que consumen más tiempo de CPU o memoria. Con esta información, pueden optimizar estas áreas para mejorar la eficiencia general del programa. Esto es especialmente crítico en entornos donde los recursos son limitados o costosos.

- **Detección de cuellos de botella**: El profiling ayuda a descubrir cuellos de botella en el código, que son las operaciones que limitan el rendimiento del programa. A menudo, estos cuellos de botella no son evidentes a simple vista y solo se revelan a través de un análisis detallado.

- **Calidad del software**: Una aplicación que utiliza recursos de manera eficiente tiende a ser más rápida y confiable. Esto mejora la experiencia del usuario y puede ser un factor determinante en el éxito de una aplicación.

- **Escalabilidad**: Comprender el comportamiento de un programa en términos de uso de memoria y tiempo de CPU es fundamental para escalar aplicaciones. El profiling proporciona información crucial que puede guiar la refactorización del código para manejar una mayor carga de trabajo.

- **Resolución de problemas**: A menudo, los problemas de rendimiento solo se manifiestan bajo ciertas condiciones de carga o datos. El profiling bajo estas condiciones puede ayudar a replicar y diagnosticar problemas que de otra manera serían esporádicos o difíciles de encontrar.

- **Costo-efectividad**: En la era de la computación en la nube, donde los recursos se pagan según se consumen, el profiling puede tener un impacto directo en la reducción de costos operativos al permitir que las aplicaciones hagan un uso más eficiente de la infraestructura.


## Actividad:

- Generar una Tabla que resuma los tipos de datos mutables e inmutables segun los siguientes lenguajes: Python, golang, java, y C++
- Elija un tipo de dato diferente a los de clase y Desarrolle un ejemplo en cada lenguaje que demuestre el por que el tipo de datos es mutable o inmutable
- Calcular la complejidad computacional de cada Algoritmo
- Hacer profiling de cala Algoritmo
- Graficar los resultados obtenidos
- Escribir los Algoritmos de este taller en Java
- Hacer profiling a los Algoritmos en Java
- Reportar los resultados obtenidos generando tablas de comparacion del resultado del profiling (tiempo y memoria) obtenidos en java y python

> Algoritmo 1

```
from memory_profiler import profile


@profile
def imprimir(lista):
    n = lista
    print(lista)


if "__main__" == __name__:
  lista = 2
  imprimir(lista)
```


> Algoritmo 2
```
from memory_profiler import profile

@profile
def mi_algoritmo(n):
    lista = list(range(n))
    pares = []
    for i in lista:
        for j in lista:
            pares.append((i, j))
    return pares

if __name__ == "__main__":
    mi_algoritmo(1000)
```



> Algoritmo 3
```
import random
import time
from memory_profiler import profile

@profile
def operacion_intensiva_memoria(n):
    """Operación que genera un gran uso de memoria temporalmente."""
    gran_lista = [random.random() for _ in range(n)]
    time.sleep(1)  # Simulamos un procesamiento
    return sum(gran_lista)

@profile
def operacion_intensiva_cpu(n):
    """Operación que consume tiempo de CPU."""
    contador = 0
    for _ in range(n):
        contador += random.random()
        time.sleep(0.01)  # Añade un pequeño retardo para simular procesamiento

def main():
    n = 500000
    for i in range(5):
        print(f"Pico {i+1}: Operación intensiva en memoria")
        operacion_intensiva_memoria(n)
        print(f"Pico {i+1}: Operación intensiva en CPU")
        operacion_intensiva_cpu(100)

if __name__ == "__main__":
    main()
```



> Algoritmo 4
```
def busqueda(arr, elemento_buscado):
    izquierda, derecha = 0, len(arr) - 1
    while izquierda <= derecha:
        medio = (izquierda + derecha) // 2
        medio_valor = arr[medio]
        
        if medio_valor == elemento_buscado:
            return medio
        elif elemento_buscado < medio_valor:
            derecha = medio - 1
        else:
            izquierda = medio + 1
    return -1

indice = busqueda([1, 2, 3, 4, 5, 6, 7, 8, 9], 7)
```

> Algoritmo 5

```
def generar_subconjuntos(conjunto):
    subconjuntos = [[]]  # Inicializa con el conjunto vacío
    for elemento in conjunto:
        nuevos_subconjuntos = []
        for subconjunto in subconjuntos:
            nuevo_subconjunto = subconjunto[:]  # Crea una copia del subconjunto actual
            nuevo_subconjunto.append(elemento)  # Agrega el elemento actual al nuevo subconjunto
            nuevos_subconjuntos.append(nuevo_subconjunto)  # Agrega el nuevo subconjunto a la lista de nuevos subconjuntos
        subconjuntos.extend(nuevos_subconjuntos)  # Agrega todos los nuevos subconjuntos a la lista de subconjuntos
    return subconjuntos

# Ejemplo de uso
conjunto = [1, 2, 3]
subconjuntos = generar_subconjuntos(conjunto)
print("Subconjuntos:", subconjuntos)

```


> [Colab](URhttps://colab.research.google.com/drive/1GdcDMWsbEPllTEVb93_mXnsPm07eHyu-?usp=sharing)
> https://colab.research.google.com/drive/1GdcDMWsbEPllTEVb93_mXnsPm07eHyu-?usp=sharing



